import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import firebase from 'firebase';

/**
 * Generated class for the UserListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user-list',
  templateUrl: 'user-list.html',
})
export class UserListPage {
  public listUsers: any = [];
  public title: string;

  constructor(public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public navParams: NavParams) {
    this.title = "Lista de usuarios";
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserListPage');
    this.getUserList();
  }

  getUserList() {

    const loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

    loading.present();

    let th = this;
    const userList = firebase.database().ref('usuarios/');
    userList.on('value', function (res) {
      console.log(res.val())
      loading.dismiss();
      if(res.val() != null)
      {
        th.listUsers.push(res.val());
      }
    });
  }

}
