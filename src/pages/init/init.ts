import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, Slides, SegmentButton, ModalController } from 'ionic-angular';
import firebase from 'firebase';

/**
 * Generated class for the InitPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-init',
  templateUrl: 'init.html',
})
export class InitPage {
  @ViewChild('sliderLogin') sliderComponent: Slides;
  public selectedSegment = '0';

  constructor(public navCtrl: NavController,
    public menuCtrl: MenuController,
    public modalCtrl: ModalController,
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InitPage');
    // oculto el menu desplegable
    this.menuCtrl.enable(false);
  }

  // Segment and slider functions

  onSegmentChanged(segmentButton: SegmentButton) {
    if (segmentButton != undefined) {
      console.log('Segment changed to', segmentButton.value);
      this.sliderComponent.slideTo(Number(segmentButton.value))
    }
    // this.sliderComponent.slideTo(index);
  }

  onSlideChanged(s: Slides) {
    console.log('Slide changed', s);
    this.selectedSegment = String(s.realIndex);
  }

  updatePerson(firstName: string, lastName: string): void {
    const personRef: firebase.database.Reference = firebase.database().ref(`/usuarios/`);
    personRef.update({
      firstName,
      lastName
    })
  }
}