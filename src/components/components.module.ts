import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegistryComponent } from './registry/registry';
import { LoginComponent } from './login/login';
@NgModule({
	declarations: [RegistryComponent,
    LoginComponent],
	imports: [
		IonicPageModule.forChild(RegistryComponent),  // <- Add
		IonicPageModule.forChild(LoginComponent)  // <- Add
		
	],
	exports: [RegistryComponent,
    LoginComponent]
})
export class ComponentsModule { }
