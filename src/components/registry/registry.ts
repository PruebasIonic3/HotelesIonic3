import { Component } from '@angular/core';
import firebase from 'firebase';
import { LoadingController } from 'ionic-angular';

/**
 * Generated class for the RegistryComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'registry',
  templateUrl: 'registry.html'
})
export class RegistryComponent {

  constructor(public loadingCtrl: LoadingController) {
    console.log('Hello RegistryComponent Component');
  }
  // Firebase function
  createPerson(username: string, email: string, password: string): void {
    const loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

    loading.present();

    setTimeout(() => {
      const personRef: firebase.database.Reference = firebase.database().ref(`/users/${username}`);
      personRef.set({
        username,
        email,
        password
      })
      loading.dismiss();
    }, 400);
   
  }
}
