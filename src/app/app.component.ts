
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import firebase from 'firebase';
import { HeaderColor } from '@ionic-native/header-color';


@Component({
  templateUrl: 'app.html'
})
export class MyApp { 
  @ViewChild(Nav) nav: Nav;

  rootPage: any = 'UserListPage';

  pages: Array<{ title: string, component: any }>;

  constructor(public platform: Platform,
    public headerColor: HeaderColor,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen) {

    firebase.initializeApp({
      apiKey: "AIzaSyB0YcmAAlW7fkXwG3cT6YYGD2mXW6bJQdc",
      authDomain: "hoteles-c84ad.firebaseapp.com",
      databaseURL: "https://hoteles-c84ad.firebaseio.com",
      projectId: "hoteles-c84ad",
      storageBucket: "hoteles-c84ad.appspot.com",
      messagingSenderId: "275757352697"
    });
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: 'HomePage' },
      { title: 'Ingresar', component: 'InitPage' },
      { title: 'Lista de usuarios', component: 'UserListPage' }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      // para cambiar el color del header de la multitarea
      this.headerColor.tint('#30404F');
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
